#!/bin/bash
################## FUNCIONS ###################
## Colorear las salidas de echo
function color ()
{
	echo $1 | ccze -A
}
#
#
## Convertir las entradas en formato markdown escapando y resaltando las lineas
## que empiezan por #, $ y <
function _mark ()
{
    cat $DIR/file1.txt | sed -e 's/^[#]/>\\#/' -e 's/^[$]/>$/' -e 's/^[<]/\t\</'  >> file1.md
    echo
    color "... Convertidos archivos a markdown..."
    echo
}
#
#
## Desglosar el tractatus en entradas independientes
function _desglose ()
{
	cat ../file1.md | awk 'BEGIN {ESTADO=1} \
		{ \
			if (ESTADO == 1 && NF == 0) \
					{ESTADO=1} \
			else    {       if (ESTADO==1 && NF != 0) {NOMBRE=$1; ESTADO=2; print $0 >> NOMBRE} \
							else    {       if (ESTADO==2 && NF == 0) {ESTADO=1} \
											else { print $0 >> NOMBRE } \
									} \
					} \
		}'
	echo
	color  "<< ....Desglose del tractatus terminado.... >>"
}
#
#
## Convertir a html
function _post() {
cd $FILES

for i in *
do
	cat $i | sed G > ../tmp/$i
        cat ../tmp/$i | sed '1i\<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>\' | sed '2i\ \' > ../tmp2/$i
	pandoc -o ../HTMLS/$i.html ../tmp2/$i
	echo "<a href="http://crontux.hopper.pw/tractatus">Volver al índice</a>" >> ../HTMLS/$i.html
        i=i+1
done
echo
color "... convertidos archivos a html..."
}
#
#
################# INICI ######################
#
## Comprobar privilegios
if [ "$(id -u)" = "0" ]
then
	echo
	color "Ejecutar el script como usuario sin privilegios..."
	echo
	exit 0
fi
## Crear directorios de trabajo.
DIR=~/WebTrac
FILES=(`date +%s`)
if [ -d $DIR ]
then
	rm -R $DIR
else
	echo
fi
mkdir -p $DIR/{$FILES,HTMLS,tmp,tmp2}
cd $DIR
#touch index.html
## Descargar el tractatus.txt
cp ~/tractatus/tractatus.txt ~/Dropbox/Dades/tractatus/1-tractatus.txt
cat ~/tractatus/tractatus.txt | sed '1d' > file1.txt
_mark
cd $FILES
_desglose
cd ..
echo
pwd
_post
cd ..
echo
# eliminar directorios y ficheros temporales:
rm -R $FILES tmp tmp2 file1.*
# copiar a la carpeta web:
cp -fu HTMLS/* /var/www/html/tractatus/
echo
ENTRADAS=$(ls /var/www/html/tractatus/ | wc -l)
echo "Hi ha $ENTRADAS entradas"
echo
exit 0
