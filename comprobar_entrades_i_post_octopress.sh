#!/bin/bash
echo
ls ~/tractatuslapipaplena/octotrac/*/ | sed 's/_/-/g' | tr 'A-Z' 'a-z' | sed 's/[-]*$//' | sed 's/ó/o/g' | sed 's/ñ/n/g' > ~/comprobar_entrades.txt
ls ~/tractatuslapipaplena/source/_posts/ | cut -c12- | cut -d . -f 1 | sed 's/_/-/g' | sed 's/-dot-/./g' | sed 's/-plus-/+/g'  | sort > ~/comprobar_posts.txt
diff ~/comprobar_entrades.txt ~/comprobar_posts.txt > ~/diferencies_entrades_post.txt
if [ -s ~/diferencies_entrades_post.txt ]
then
    echo "Hi ha diferencies"
else
    echo "Iguals!!"
fi
echo
rm ~/comprobar_*.txt
exit 0
