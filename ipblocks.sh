#!/bin/bash
# el listado de ips ha de colocarse en el archivo blockips.txt
function reglas_iptables ()
{
	echo
	iptables -nL | grep DROP | tee -a reglas_drop.txt
	echo
}
function bloquear_listado ()
{
read -p "<< ¿Bloquear por iptables las ips guardadas en blockips.txt? [s/n] >>" BLOQUEAR
	echo
	if [ $BLOQUEAR = s ]
	then
		for i in $(cat blockips.txt); do
			iptables -A INPUT -s $i -j DROP
		done
	else
		echo
		echo "<< No existen reglas en iptables... >>" | ccze -A
	fi
}
###
if [ "$(id -u)" != "0" ]; then
	echo
	echo "<< Solo root puede ejecutar este script. >>" | ccze -A
	echo
	exit 1
fi
clear
echo
echo "<< Las reglas actuales de iptables son:  >>" | ccze -A
reglas_iptables
if [ -s reglas_drop.txt ]
then
	echo
	read -p "<< ¿Resetear todas las reglas de iptables? [s/n] >>" RESETEAR
	echo
	if [ $RESETEAR = s ]
	then
		iptables -P INPUT ACCEPT
		iptables -P FORWARD ACCEPT
		iptables -P OUTPUT ACCEPT
		iptables -F
		iptables -X
		echo "<< Reglas reseteadas.... >>"	| ccze -A
		echo
		bloquear_listado
	else
		echo
		echo  "<< Se conservan las reglas existentes.... >>" | ccze -A
		echo
		reglas_iptables
		echo
	fi
else
	echo
	echo "<< No existen reglas en iptables...>>" | ccze -A
	echo
	bloquear_listado
	reglas_iptables
fi
#clear
rm reglas_drop.txt
echo
exit
