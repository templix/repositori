#!/bin/bash
# Date: 27-04-2016
# Author: "lapipaplena" <lapipaplena@gmail.com>
# Version: 5.0
# Licence: GPL v3.0
# Description: Busca ips aleatorias con puertos abiertos
# Require: nmap ccze dialog
####
if [ "$(id -u)" != "0" ]; then
	dialog --title "INFO" --msgbox "Solo root puede ejecutar este script." 5 43
	clear
	exit 1
fi
#
USER=$(cat /etc/passwd | grep 1000 | cut -d : -f 6)
DIR=$USER/incauts
if [ -d $DIR ]
then
    echo
else
	mkdir $DIR
fi
clear
OPC=s
while [ $OPC = s ]
do
    echo
    echo "[1] Escanear ips aleatorias"
    echo "[2] Trabajar sobre archivos"
    echo "[3] Salir"
    echo
    read -n 1 -p "Ingrese una opción: " OPCION
    echo
    case $OPCION in
        1 ) ## Escanear ips aliatorias
            DATE=$(date +"%T" | tr -d :)
            mkdir $DIR/$DATE
            cd $DIR/$DATE
            NUM=$(shuf -i 1-1000 -n 1)
            echo
            read -p "<< ¿Numero de ips a escanear? (50, 100, 1000...) >> " IPS
            echo
            read -p "<< ¿Filtrar por el PUERTO? (22, 80, 445, 5900...) >> " PORT
            echo
            read -p "<< ¿Filtrar el ESTADO? (filtered, open, closed...) >> " STAT
            echo
            clear
            echo
            echo "<< Se procede a escanear $IPS ips que tengan el puerto $PORT ---> $STAT...>>" | ccze -A
            echo
            nmap -iR $IPS -p $PORT  > nmap.txt 2>/dev/null
            cat nmap.txt | sed -e '/Starting/ d' | sed -e \$d  > nmap1.txt
            cat nmap1.txt | grep report | awk '{print $NF}' | cut -d "(" -f2 | cut -d ")" -f1 > nmap2.txt
            cat nmap1.txt | grep $PORT/tcp | awk '{print $2}' > nmap3.txt
            paste -d " " nmap2.txt nmap3.txt > nmap4.txt
            cat nmap4.txt | grep '^[0-9]' | grep $STAT | awk '{print$1}' | cut -d : -f 1 > ${NUM}-IpOpen.txt
            clear
            echo
            rm nmap*.txt
            echo "<< Las IPs que cumplen los requisitos solicitados son: >>" | ccze -A
            echo
            if [ -s ${NUM}-IpOpen.txt ]; then
                cat ${NUM}-IpOpen.txt
                echo
            else
                clear
                echo
                echo "No se han encontrado ips que cumplan los requisitos, abortando..." | ccze -A
                rm -R $DIR/$DATE
                echo
                break
                #exit 1
            fi
            echo
            echo "<< Pulsar intro para continuar....>>" | ccze -A
            read
            read -n 1 -p "<< ¿Proseguir el ataque al listado con los plugins de nmap? [s/n] >>" OP
            sortir=0
            if [ $OP = s ]
            then
                #clear
	            while [ $sortir -eq 0 ];
	            do
		            echo
                    ls -1 /usr/share/nmap/scripts | cut -d . -f 1 > ${NUM}-modulos.txt
                    echo
                    read -p "¿Buscar modulos con el servicio? (ssh,http,ftp) " SERVICIO
                    modu=($(cat ${NUM}-modulos.txt | grep $SERVICIO))
		            declare -p modu | sed -e 's/ /\n/g'
		            read MOD
                    touch ${NUM}-${MOD}-resul_nmap.txt
		            clear
		            echo
		            echo "<< Procesando con el módulo: >>"
                    echo
                    echo "${modu[MOD]}" | tee ${NUM}-modulo.txt
		            echo
		            MODU=$(cat ${NUM}-modulo.txt)
		            for linea in `cat ${NUM}-IpOpen.txt`
                    do
			            let numero+=1
			            nmap -O -sS --script=$MODU -P0 $linea -p T:$PORT  >> ${NUM}-${MOD}-resul_nmap.txt
			            echo "---------------------------------------" >> ${NUM}-${MOD}-resul_nmap.txt
		            done
		            echo
		            echo  "<< Escaneo  finalizado.... >>" | ccze -A
		            echo
		            read -n 1 -p "<< ¿Visualizar los datos? [s/n] >>" OP1
		            if  [ $OP1 = s ]
		            then
			            if [ -s ${NUM}-${MOD}-resul_nmap.txt ]
			            then
				            echo
				            cat ${NUM}-${MOD}-resul_nmap.txt
				            echo
			            else
                            clear
                            echo
				            echo "No se han encontrado datos, abortando...."
				            exit 1
			            fi
			            echo "<< Pulsar intro para continuar....>>" | ccze -A
			            read
		            else
			            echo
		            fi
		            clear
		            echo
		            read -n 1 -p "¿Desea provar otro script de ataque? [s/n]" CONTINUAR
		            echo
		            if  [ $CONTINUAR != "s" ];
		            then
			            break
                    fi
	            done
            else
	            echo
            fi
            clear
            echo
            echo "<< Las ips se guardan en el archivo ${DIR}/${NUM}-IpOpen.txt >>" | ccze -A
            echo
            chmod  -R 777 $DIR/*
            ;;
        2 ) ## Trabajar sobre archivos
            cd ${DIR}
            VER=s
            while [ $VER = s ]
            do
                clear
                echo
                CARPETAS=($(ls))
		        declare -p CARPETAS | sed -e 's/ /\n/g' | cut -d "(" -f 2 | sed -n '3,$p'
                read DIRE
                if [ -d "${CARPETAS[DIRE]}" ]; then
                    echo
                else
                    echo
                    echo "No existe el directorio ${CARPETAS[DIRE]}"
                    echo
                    break
                fi
                clear
                ARCHIVOS=($(ls ${CARPETAS[DIRE]}))
                declare -p ARCHIVOS | sed -e 's/ /\n/g' | cut -d "(" -f 2 | sed -n '3,$p'
                read ARCH
                if [ -f "${CARPETAS[DIRE]}/${ARCHIVOS[ARCH]}" ]; then
                    echo
                else
                    echo
                    echo "No existe el fichero ${ARCHIVOS[ARCH]}"
                    echo
                    break
                fi
                clear
                nano ${CARPETAS[DIRE]}/${ARCHIVOS[ARCH]}
                echo
                read -n 1 -p "¿Consultar otro directorio? (s/n) " VER
                echo
            done
            ;;
        3 ) ## salir
            clear
            echo
            exit 0
            ;;
        * ) # cualquier otra opción
            clear
            echo
            echo "Opción no contemplada"
            echo
            ;;
    esac
    echo
    read -n 1 -p "¿Volver al menú (s/n) " OPC
    echo
done
echo
clear
exit 0
